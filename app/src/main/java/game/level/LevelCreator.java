package game.level;

import android.graphics.Paint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Stack;

public class LevelCreator {
    public static int levelHeight = 14;
    public static int levelWidth = 8;
    private Random randomGenerator;
    private Cell[][] map;
    private int[][] directions;
    private ArrayList<Integer> uniqueNumbers;
    private Cell end;
    private Player digger;

    public LevelCreator() {
        randomGenerator = new Random();
        map = new Cell[levelHeight][levelWidth];
        directions = new int[][]{{1, -1, 0, 0}, {0, 0, -1, 1}};
        uniqueNumbers = new ArrayList<>();
        for (int i = 0; i < levelHeight; i++) {
            for (int j = 0; j < levelWidth; j++) {
                Cell cell = new Cell(i, j);
                map[i][j] = cell;
            }
        }
        for (int i = 0; i < 4; i++) {
            uniqueNumbers.add(new Integer(i));
        }
    }

    public void start(int startY, int startX) {
        digger = new Player(startY, startX, new Paint());
        createMaze(map[startY][startX]);
        clearMaze();
    }

    private void clearMaze() {
        for(int i=0; i<levelHeight; i++) {
            for(int j=0; j<levelWidth; j++) {
                map[i][j].setVisitedBy(null);
            }
        }
    }

    private void createMaze(Cell cell) {
        Stack<Cell> wayBack = new Stack<>();
        int farthestCellDistance = 0;
        int distance = 0;
        while (true) {
            boolean foundWay = false;
            cell.setVisitedBy(digger);
            Collections.shuffle(uniqueNumbers);
            for (int i = 0; i < uniqueNumbers.size(); i++) {
                int y = cell.getY() + directions[0][uniqueNumbers.get(i)];
                int x = cell.getX() + directions[1][uniqueNumbers.get(i)];
                if (y >= 0 && y < levelHeight && x >= 0 && x < levelWidth && !map[y][x].isVisited()) {
                    cell.breakWallTo(map[y][x]);
                    distance++;
                    wayBack.push(cell);
                    cell = map[y][x];
                    foundWay = true;
                    break;
                }
            }
            if (!wayBack.empty() && !foundWay) {
                if (distance > farthestCellDistance) {
                    farthestCellDistance = distance;
                    end = cell;
                }
                cell = wayBack.pop();
                distance--;
            }
            if (wayBack.empty() && !foundWay) {
                break;
            }
        }
    }

    public Cell[][] getMap() {
        return map;
    }

    public Cell getEnd() {
        return end;
    }
}
