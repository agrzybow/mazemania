package game.level;

import android.animation.ObjectAnimator;
import android.graphics.Paint;

public class Player {
    private int x;
    private int y;
    private float drawX;
    private float drawY;
    private Paint paint;
    private int pointsCollected;
    private int distance;
    private int cellsVisited;
    private int directionX;
    private int directionY;

    public Player(int y, int x, Paint paint) {
        this.y = y;
        this.x = x;
        this.drawX = x * Cell.cellWidth;
        this.drawY = y * Cell.cellHeight;
        this.paint = paint;
        paint.setStrokeWidth(10);
        this.pointsCollected = 0;
        this.distance = 0;
        this.cellsVisited = 0;
    }

    public void addPointsCollected() {
        this.pointsCollected++;
    }

    public void addDistance() {
        this.distance++;
    }

    public int getDistance() {
        return distance;
    }

    public int getPointsCollected() {
        return pointsCollected;
    }

    public Paint getPaint() {
        return paint;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        this.drawY = y * Cell.cellHeight;
    }

    public void setX(int x) {
        this.x = x;
        this.drawX = x * Cell.cellWidth;
    }

    public float getDrawX() {
        return drawX;
    }

    public float getDrawY() {
        return drawY;
    }

    public int getCellsVisited() {
        return cellsVisited;
    }

    public void addCellsVisited() {
        this.cellsVisited++;
    }

    public void subCellsVisited() {
        this.cellsVisited--;
    }

    public void setDirectionX(int directionX) {
        this.directionX = directionX;
    }

    public void setDirectionY(int directionY) {
        this.directionY = directionY;
    }

    public int getDirectionX() {
        return directionX;
    }

    public int getDirectionY() {
        return directionY;
    }
}
