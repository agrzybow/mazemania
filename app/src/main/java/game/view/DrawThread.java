package game.view;

import android.graphics.Canvas;
import android.view.SurfaceHolder;


public class DrawThread extends Thread {
    private SurfaceHolder surfaceHolder;
    private DrawView drawView;
    private boolean running;

    public DrawThread(SurfaceHolder surfaceHolder, DrawView drawView) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.drawView = drawView;
    }

    @Override
    public void run() {
        long startTime = System.nanoTime();

        while (running) {
            Canvas canvas = null;
            try {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (canvas) {
                    this.drawView.draw(canvas);
                }
            } catch(Exception e) {
                e.printStackTrace();
            } finally {
                if(canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            long now = System.nanoTime() ;
            long waitTime = (now - startTime)/1000000;
            if(waitTime < 10)  {
                waitTime= 10; // Millisecond.
            }

            try {
                this.sleep(waitTime);
            } catch(InterruptedException e)  {

            }
            startTime = System.nanoTime();
        }

    }
    public void setRunning(boolean running) {
        this.running = running;
    }
}
