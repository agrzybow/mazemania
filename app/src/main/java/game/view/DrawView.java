package game.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.adamgrzybowski.mazemania.R;

import java.text.SimpleDateFormat;

import game.level.Cell;
import game.level.LevelCreator;
import game.level.Player;
import game.level.Point;
import game.modes.GameMode;

public class DrawView extends SurfaceView implements SurfaceHolder.Callback {
    DrawThread drawThread = null;
    GameMode gameMode;

    private Player player;
    int playerRadius;
    private Player[] opponents;
    private Cell[][] map;
    private Cell end;

    Paint wallPaint;
    Paint playerPaint;
    Paint timePaint;
    int backgroundColor;

    Bitmap diamond;
    Bitmap exit;

    SimpleDateFormat timeFormat;

    public DrawView(Context context, GameMode gameMode) {
        super(context);
        this.map = gameMode.getMap();
        this.end = gameMode.getEnd();
        this.player = gameMode.getPlayer();
        this.opponents = gameMode.getBots();
        this.gameMode = gameMode;
        this.timeFormat = new SimpleDateFormat("mm:ss");

        if(Cell.cellWidth > Cell.cellHeight) {
            playerRadius = Cell.cellHeight/2;
        } else {
            playerRadius = Cell.cellWidth/2;
        }

        wallPaint = new Paint();
        playerPaint = new Paint();
        timePaint = new Paint();
        backgroundColor = getResources().getColor(R.color.colorBackground);

        wallPaint.setColor(Color.WHITE);
        wallPaint.setStrokeWidth(3);
        playerPaint.setColor(Color.BLUE);
        timePaint.setColor(Color.BLACK);
        timePaint.setTextSize(Cell.cellHeight);

        diamond = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.diamond), Cell.cellWidth/2, Cell.cellHeight/2, false);
        exit = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.exit), Cell.cellWidth, Cell.cellHeight, false);


        setFocusable(true);
        getHolder().addCallback(this);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawColor(backgroundColor);
        for (int i = 0; i < LevelCreator.levelHeight; i++) {
            for (int j = 0; j < LevelCreator.levelWidth; j++) {
                Cell cell = map[i][j];
                if (cell.getClass().equals(Point.class) && !((Point) cell).isCollected()) {
                    canvas.drawBitmap(diamond, cell.getDrawX() + Cell.cellWidth / 4, cell.getDrawY() + Cell.cellHeight / 4, null);
                }
                if (cell.isVisited()) {
                    canvas.drawPoint(cell.getDrawX() + Cell.cellWidth / 2, cell.getDrawY() + Cell.cellHeight / 2, cell.getVisitedBy().getPaint());
                }
                if (cell.isWallUp()) {
                    canvas.drawLine(cell.getDrawX(), cell.getDrawY(), cell.getDrawX() + Cell.cellWidth, cell.getDrawY(), wallPaint);
                }
                if (cell.isWallDown()) {
                    canvas.drawLine(cell.getDrawX(), cell.getDrawY() + Cell.cellHeight, cell.getDrawX() + Cell.cellWidth, cell.getDrawY() + Cell.cellHeight, wallPaint);
                }
                if (cell.isWallLeft()) {
                    canvas.drawLine(cell.getDrawX(), cell.getDrawY(), cell.getDrawX(), cell.getDrawY() + Cell.cellHeight, wallPaint);
                }
                if (cell.isWallRight()) {
                    canvas.drawLine(cell.getDrawX() + Cell.cellWidth, cell.getDrawY(), cell.getDrawX() + Cell.cellWidth, cell.getDrawY() + Cell.cellHeight, wallPaint);
                }
            }
        }

        if(end!=null) {
            canvas.drawBitmap(exit, end.getDrawX(), end.getDrawY(), null);
        }

        for (Player opponent : opponents) {
            canvas.drawRect(opponent.getDrawX(), opponent.getDrawY(), opponent.getDrawX() + Cell.cellWidth, opponent.getDrawY() + Cell.cellHeight, opponent.getPaint());
        }

        canvas.drawCircle(player.getDrawX() + Cell.cellWidth / 2, player.getDrawY() + Cell.cellHeight / 2, playerRadius, playerPaint);

        canvas.drawText(timeFormat.format(gameMode.getTime()), Resources.getSystem().getDisplayMetrics().widthPixels / 2 - timePaint.getTextSize(), Cell.cellHeight, timePaint);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        drawThread = new DrawThread(getHolder(), this);
        drawThread.setRunning(true);
        drawThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        try {
            drawThread.setRunning(false);
            drawThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void onDestroy() {
        drawThread.setRunning(false);
    }
}
