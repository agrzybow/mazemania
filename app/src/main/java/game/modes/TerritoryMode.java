package game.modes;

import android.os.CountDownTimer;

import game.others.Controls;
import game.bot.BotThread;
import game.bot.RandomBotThread;
import game.level.Cell;
import game.level.Player;
import game.others.Trail;

public class TerritoryMode extends GameMode {
    private long timeLimit = 90000;
    private long actualTime = 0;
    private CountDownTimer countDownTimer;

    public TerritoryMode(Cell[][] map, Cell end, int playerY, int playerX, int pointsNumber, int opponentsNumber) {
        super(map, end, playerY, playerX, pointsNumber, opponentsNumber);

        countDownTimer = new CountDownTimer(timeLimit,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                actualTime = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                actualTime = 0;
            }
        }.start();
    }

    @Override
    public void putTrail(Player player, Cell current, Cell next) {
        Trail.basic(player, current, next);
    }

    @Override
    public GameStatus checkGameStatus() {
        if(actualTime == 0) {
            for (Player opponent : bots) {
                if (opponent.getCellsVisited() > getPlayer().getCellsVisited()) {
                    return GameStatus.LOSE;
                }
            }
            return GameStatus.WIN;
        } else {
            return GameStatus.CONTINUE;
        }
    }

    @Override
    public long getTime() {
        return actualTime;
    }

    @Override
    public void onDestroy() {
        countDownTimer.cancel();
    }

    @Override
    public int getWage() {
        return 4;
    }

    @Override
    public int getMode() {
        return territoryMode;
    }

    @Override
    public BotThread createBotThread(Controls botControls, Player bot) {
        return new RandomBotThread(botControls, bot);
    }
}
