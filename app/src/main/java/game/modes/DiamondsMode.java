package game.modes;

import game.others.Controls;
import game.bot.BotThread;
import game.level.Cell;
import game.level.Player;
import game.others.Trail;

public class DiamondsMode extends GameMode {
    public DiamondsMode(Cell[][] map, Cell end, int playerY, int playerX, int pointsNumber, int opponentsNumber) {
        super(map, end, playerY, playerX, pointsNumber, opponentsNumber);
    }


    @Override
    public void putTrail(Player player, Cell current, Cell next) {
        Trail.basic(player, current, next);
    }

    @Override
    public GameStatus checkGameStatus() {
        if(map[player.getY()][player.getX()].equals(end) && points.length == getPlayer().getPointsCollected()) {
            return GameStatus.WIN;
        } else {
            return GameStatus.CONTINUE;
        }
    }

    @Override
    public long getTime() {
        return System.currentTimeMillis() - gameStartTime;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getWage() {
        return 2;
    }

    @Override
    public int getMode() {
        return diamondsMode;
    }

    @Override
    public BotThread createBotThread(Controls botControls, Player bot) {
        return null;
    }
}
