package game.others;

import game.level.Cell;
import game.level.Player;

public class Trail {
    public static void basic(Player player, Cell current, Cell next) {
        current.setVisitedBy(player);
    }

    public static void followPlayer(Player player, Cell current, Cell next) {
        if(next.getVisitedBy() != null && next.getVisitedBy().equals(player)) {
            current.setVisitedBy(null);
        } else {
            current.setVisitedBy(player);
        }
    }
}
