package game.bot;

import game.others.Controls;
import game.level.Player;

public abstract class BotThread implements Runnable {
    Controls botControls;
    Player bot;

    BotThread(Controls botControls, Player bot) {
        this.botControls = botControls;
        this.bot = bot;
    }

    public abstract void algorithm();

    @Override
    public void run() {
        if (bot.getDirectionY() == 0 && bot.getDirectionX() == 0) {
            algorithm();
        }
    }
}
