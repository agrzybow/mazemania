package game.bot;

import java.util.Random;

import game.others.Controls;
import game.level.Player;

public class RandomBotThread extends BotThread {
    private Random randomGenerator;
    private int[][] directions;

    public RandomBotThread(Controls botControls, Player bot) {
        super(botControls, bot);
        randomGenerator = new Random();
        directions = new int[][]{{1, -1, 0, 0}, {0, 0, -1, 1}};

    }

    @Override
    public void algorithm() {
        int direction = randomGenerator.nextInt(4);
        bot.setDirectionY(directions[0][direction]);
        bot.setDirectionX(directions[1][direction]);

        if(botControls.checkNextIsVisited(bot.getDirectionY(), bot.getDirectionX())) {
            botControls.tryToChangeDirection(bot.getDirectionY(), bot.getDirectionX());
        }

    }
}
