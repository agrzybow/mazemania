package com.example.adamgrzybowski.mazemania;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import game.modes.GameMode;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

    }

    private void startIntent(int mode) {
        Intent intent = new Intent(this, CustomizationActivity.class);
        intent.putExtra("MODE", mode);
        startActivity(intent);
    }

    public void classicMazeModeButton(View view) {
        startIntent(GameMode.classicMazeMode);
    }

    public void diamondsModeButton(View view) {
        startIntent(GameMode.diamondsMode);
    }

    public void escapeModeButton(View view) {
        startIntent(GameMode.escapeMode);
    }

    public void raceModeButton(View view) {
        startIntent(GameMode.raceMode);
    }

    public void territoryModeButton(View view) {
        startIntent(GameMode.territoryMode);
    }
}
