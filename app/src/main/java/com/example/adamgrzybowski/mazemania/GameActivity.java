package com.example.adamgrzybowski.mazemania;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import game.others.Controls;
import game.others.DatabaseHelper;
import game.others.Supervisor;
import game.bot.BotThread;
import game.level.LevelCreator;

import game.modes.ClassicMazeMode;
import game.modes.DiamondsMode;
import game.modes.EscapeMode;
import game.modes.GameMode;
import game.modes.RaceMode;
import game.modes.TerritoryMode;
import game.view.DrawView;

public class GameActivity extends Activity {
    private GestureDetector gestureDetector;
    private DrawView drawView;
    private GameMode gameMode;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        int enemies = getIntent().getIntExtra("ENEMIES", 0);
        int mode = getIntent().getIntExtra("MODE", 0);
        int points = getIntent().getIntExtra("POINTS", 0);

        databaseHelper = new DatabaseHelper(this);

        int playerY = LevelCreator.levelHeight / 2;
        int playerX = LevelCreator.levelWidth / 2;
        LevelCreator creator = new LevelCreator();
        creator.start(playerY, playerX);

        switch (mode) {
            case GameMode.classicMazeMode:
                gameMode = new ClassicMazeMode(creator.getMap(), creator.getEnd(), playerY, playerX, 0, 0);
                break;
            case GameMode.diamondsMode:
                gameMode = new DiamondsMode(creator.getMap(), creator.getEnd(), playerY, playerX, points, 0);
                break;
            case GameMode.escapeMode:
                gameMode = new EscapeMode(creator.getMap(), creator.getEnd(), playerY, playerX, 0, enemies);
                break;
            case GameMode.raceMode:
                gameMode = new RaceMode(creator.getMap(), null, playerY, playerX, points, enemies);
                break;
            case GameMode.territoryMode:
                gameMode = new TerritoryMode(creator.getMap(), null, playerY, playerX, 0, enemies);
                break;
        }

        drawView = new DrawView(this, gameMode);
        Controls playerControls = new Controls(gameMode, gameMode.getPlayer());

        Controls[] opponentsControls = new Controls[gameMode.getBots().length];
        BotThread[] botThreads = new BotThread[gameMode.getBots().length];
        if(gameMode.getBots() != null) {
            for (int i = 0; i < gameMode.getBots().length; i++) {
                opponentsControls[i] = new Controls(gameMode, gameMode.getBots()[i]);
                botThreads[i] = gameMode.createBotThread(opponentsControls[i], gameMode.getBots()[i]);
            }
        }

        Supervisor supervisor = new Supervisor(this, gameMode, botThreads, opponentsControls, playerControls);
        supervisor.start();

        gestureDetector = new GestureDetector(this, new PlayerGestureListener(playerControls, gameMode.getPlayer()));
        setContentView(drawView);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        drawView.onDestroy();
        gameMode.onDestroy();
    }

    public void win() {
        onDestroy();
        createDialog(R.layout.win_dialog, true).show();
    }

    public void lose() {
        onDestroy();
        createDialog(R.layout.lose_dialog, false).show();
    }

    private AlertDialog createDialog(int layoutID, boolean win) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(layoutID, null);
        builder.setView(view);
        final AlertDialog dialog = builder.create();

        int currentScore = gameMode.getScore();
        long endTimeMillis = gameMode.getTime();
        int endDistance = gameMode.getPlayer().getDistance();

        if(win) {
            long scoreSum = databaseHelper.getPlayerScore();
            int lvl = (int)((scoreSum + currentScore) / GameMode.scorePerLevel);
            int progress = (int)(((scoreSum + currentScore) / (GameMode.scorePerLevel/100)) % 100);

            TextView currentLevelTV = view.findViewById(R.id.current_lvl);
            TextView nextLevelTV = view.findViewById(R.id.next_lvl);
            ProgressBar progressBar = view.findViewById(R.id.progress);

            currentLevelTV.setText("LVL " + Integer.toString(lvl));
            nextLevelTV.setText("LVL " + Integer.toString(lvl+1));
            progressBar.setProgress(progress);
        }
        databaseHelper.insertNewScore(gameMode.getMode(), currentScore);
        TextView timeTV = view.findViewById(R.id.endtime);
        TextView distanceTV = view.findViewById(R.id.end_distance);

        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
        String timeString = sdf.format(endTimeMillis);

        timeTV.setText(timeString);
        distanceTV.setText(Integer.toString(endDistance));

        Button returnButton = view.findViewById(R.id.return_button);

        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        return dialog;
    }
}
